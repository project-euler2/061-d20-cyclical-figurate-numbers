#We create all the functions to find the relevant numbers
def triangle_number(n):
    return int((n * (n+1))/2)

def square_number(n):
    return int(n*n)

def pentagonal_number(n):
    return int((n * (3*n - 1)) / 2)

def hexagonal_number(n):
    return int(n * (2*n - 1))

def heptagonal_number(n):
    return int((n * (5*n - 3))/2)

def octagonal_number(n):
    return int(n * (3*n - 2))

def octo_square(octogones, squares):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                return(str(octogone)+' '+str(square))

def octo_square_tri(octogones, squares, triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_square:
                        last_two_tri = str(triangle)[2:]
                        return(str(octogone)+' '+str(square)+' '+str(triangle))

def octo_square_tri_pent(octogones, squares, triangles,pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_square:
                        last_two_tri = str(triangle)[2:]
                        for pentagone in pentagones:
                            if str(pentagone)[:2] == last_two_tri:
                                last_two_pent = str(pentagone)[2:]
                                return(str(octogone)+' '+str(square)+' '+str(triangle)+' '+str(pentagone))

def octo_square_tri_hex(octogones, squares, triangles,hexagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_square:
                        last_two_tri = str(triangle)[2:]
                        for hexagone in hexagones:
                            if str(hexagone)[:2] == last_two_tri:
                                last_two_hex = str(hexagone)[2:]
                                return(str(octogone)+' '+str(square)+' '+str(triangle)+' '+str(hexagone))

def octo_square_tri_hept(octogones, squares, triangles,heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_square:
                        last_two_tri = str(triangle)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_tri:
                                last_two_hept = str(heptagone)[2:]
                                return(str(octogone)+' '+str(square)+' '+str(triangle)+' '+str(heptagone))

def octo_square_pent(octogones, squares, pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_square:
                        last_two_pent = str(pentagone)[2:]
                        return(str(octogone)+' '+str(square)+' '+str(pentagone))

def octo_square_pent_tri(octogones, squares, pentagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_square:
                        last_two_pent = str(pentagone)[2:]
                        for triangle in triangles:
                            if str(triangle)[:2] == last_two_pent:
                                last_two_tri = str(triangle)[2:]
                                return(str(octogone)+' '+str(square)+' '+str(pentagone)+' '+str(triangle))

def octo_square_pent_hex(octogones, squares, pentagones,hexagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_square:
                        last_two_pent = str(pentagone)[2:]
                        for hexagone in hexagones:
                            if str(hexagone)[:2] == last_two_pent:
                                last_two_hex = str(hexagone)[2:]
                                return(str(octogone)+' '+str(square)+' '+str(pentagone)+' '+str(hexagone))

def octo_square_pent_hept(octogones, squares, pentagones,heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_square:
                        last_two_pent = str(pentagone)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_pent:
                                last_two_hept = str(heptagone)[2:]
                                return(str(octogone)+' '+str(square)+' '+str(pentagone)+' '+str(heptagone))

def octo_square_hex(octogones, squares, hexagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        return(str(octogone)+' '+str(square)+' '+str(hexagone))

def octo_square_hex_tri(octogones, squares, hexagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for triangle in triangles:
                            if str(triangle)[:2] == last_two_hex:
                                last_two_tri = str(triangle)[2:]
                                return(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(triangle))

def octo_square_hex_tri_pent(octogones, squares, hexagones,triangles,pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for triangle in triangles:
                            if str(triangle)[:2] == last_two_hex:
                                last_two_tri = str(triangle)[2:]
                                for pentagone in pentagones:
                                    if str(pentagone)[:2] == last_two_tri:
                                        last_two_pent = str(pentagone)[2:]
                                        return(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(triangle)+' '+str(pentagones))

def octo_square_hex_tri_hept(octogones, squares, hexagones,triangles,heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for triangle in triangles:
                            if str(triangle)[:2] == last_two_hex:
                                last_two_tri = str(triangle)[2:]
                                for heptagone in heptagones:
                                    if str(heptagone)[:2] == last_two_tri:
                                        last_two_hept = str(heptagone)[2:]
                                        return(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(triangle)+' '+str(heptagone))

def octo_square_hex_pent(octogones, squares, hexagones,pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for pentagone in pentagones:
                            if str(pentagone)[:2] == last_two_hex:
                                last_two_pent = str(pentagone)[2:]
                                return(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(pentagone))

def octo_square_hex_pent_tri(octogones, squares, hexagones,pentagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for pentagone in pentagones:
                            if str(pentagone)[:2] == last_two_hex:
                                last_two_pent = str(pentagone)[2:]
                                for triangle in triangles:
                                    if str(triangle)[:2] == last_two_pent:
                                        last_two_tri = str(triangle)[2:]
                                        return(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(pentagone)+' '+str(triangles))

def octo_square_hex_pent_hept(octogones, squares, hexagones,pentagones,heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for pentagone in pentagones:
                            if str(pentagone)[:2] == last_two_hex:
                                last_two_pent = str(pentagone)[2:]
                                for heptagone in heptagones:
                                    if str(heptagone)[:2] == last_two_pent:
                                        last_two_hept = str(heptagone)[2:]
                                        return(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(pentagone)+' '+str(heptagone))

def octo_square_hex_hept(octogones, squares, hexagones,heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_hex:
                                last_two_hept = str(heptagone)[2:]
                                return(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(heptagone))

def octo_square_hex_hept_tri(octogones, squares, hexagones,heptagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_hex:
                                last_two_hept = str(heptagone)[2:]
                                for triangle in triangles:
                                    if str(triangle)[:2] == last_two_hept:
                                        last_two_tri = str(triangle)[2:]
                                        return(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(heptagone)+' '+str(triangle))

def octo_square_hex_hept_pent(octogones, squares, hexagones,heptagones,pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_hex:
                                last_two_hept = str(int(heptagone))[2:]
                                for pentagone in pentagones:
                                    if str(int(pentagone))[:2] == last_two_hept:
                                        last_two_pent = str(pentagone)[2:]
                                        return(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(heptagone)+' '+str(pentagone))

def octo_square_hex_hept_pent_tri(octogones, squares, hexagones,heptagones,pentagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for square in squares:
            if str(square)[:2] == last_two_octo:
                last_two_square = str(square)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_square:
                        last_two_hex = str(hexagone)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_hex:
                                last_two_hept = str(int(heptagone))[2:]
                                for pentagone in pentagones:
                                    if str(int(pentagone))[:2] == last_two_hept:
                                        last_two_pent = str(int(pentagone))[2:]
                                        for triangle in triangles:
                                            if str(int(triangle))[:2] == last_two_pent:
                                                last_two_tri = str(triangle)[2:]
                                                print(str(octogone)+' '+str(square)+' '+str(hexagone)+' '+str(heptagone)+' '+str(pentagone)+' '+str(triangle))

def octo_tri(octogones, triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for triangle in triangles:
            if str(triangle)[:2] == last_two_octo:
                last_two_tri = str(triangle)[2:]
                return(str(octogone)+' '+str(triangle))

def octo_tri_pent(octogones, triangles,pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for triangle in triangles:
            if str(triangle)[:2] == last_two_octo:
                last_two_tri = str(triangle)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_tri:
                        last_two_pent = str(pentagone)[2:]
                        return(str(octogone)+' '+str(triangle)+' '+str(pentagone))

def octo_tri_hex(octogones, triangles,hexagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for triangle in triangles:
            if str(triangle)[:2] == last_two_octo:
                last_two_tri = str(triangle)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_tri:
                        last_two_hex = str(hexagone)[2:]
                        return(str(octogone)+' '+str(triangle)+' '+str(hexagone))

def octo_tri_hept(octogones, triangles,heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for triangle in triangles:
            if str(triangle)[:2] == last_two_octo:
                last_two_tri = str(triangle)[2:]
                for heptagone in heptagones:
                    if str(heptagone)[:2] == last_two_tri:
                        last_two_hept = str(heptagone)[2:]
                        return(str(octogone)+' '+str(triangle)+' '+str(heptagone))

def octo_tri_square(octogones, triangles,squares):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for triangle in triangles:
            if str(triangle)[:2] == last_two_octo:
                last_two_tri = str(triangle)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_tri:
                        last_two_square = str(square)[2:]
                        return(str(octogone)+' '+str(triangle)+' '+str(square))

def octo_pent(octogones, pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for pentagone in pentagones:
            if str(pentagone)[:2] == last_two_octo:
                last_two_pent = str(pentagone)[2:]
                return(str(octogone)+' '+str(pentagone))

def octo_pent_tri(octogones, pentagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for pentagone in pentagones:
            if str(pentagone)[:2] == last_two_octo:
                last_two_pent = str(pentagone)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_pent:
                        last_two_tri = str(triangle)[2:]
                        return(str(octogone)+' '+str(pentagone)+' '+str(triangle))

def octo_pent_square(octogones, pentagones,squares):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for pentagone in pentagones:
            if str(pentagone)[:2] == last_two_octo:
                last_two_pent = str(pentagone)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_pent:
                        last_two_square = str(square)[2:]
                        return(str(octogone)+' '+str(pentagone)+' '+str(square))

def octo_pent_hex(octogones, pentagones,hexagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for pentagone in pentagones:
            if str(pentagone)[:2] == last_two_octo:
                last_two_pent = str(pentagone)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_pent:
                        last_two_hex = str(hexagone)[2:]
                        return(str(octogone)+' '+str(pentagone)+' '+str(hexagone))

def octo_pent_hept(octogones, pentagones,heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for pentagone in pentagones:
            if str(pentagone)[:2] == last_two_octo:
                last_two_pent = str(pentagone)[2:]
                for heptagone in heptagones:
                    if str(heptagone)[:2] == last_two_pent:
                        last_two_hept = str(heptagone)[2:]
                        return(str(octogone)+' '+str(pentagone)+' '+str(heptagone))

def octo_hex_tri(octogones, hexagones, triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_hex:
                        last_two_tri = str(triangle)[2:]
                        return(str(octogone)+' '+str(hexagone)+' '+str(triangle))

def octo_hex_tri_square(octogones, hexagones, triangles, squares):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_hex:
                        last_two_tri = str(triangle)[2:]
                        for square in squares:
                            if str(square)[:2] == last_two_tri:
                                return(str(octogone)+' '+str(hexagone)+' '+str(triangle)+' '+str(square))

def octo_hex_tri_hept(octogones, hexagones, triangles, heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_hex:
                        last_two_tri = str(triangle)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_tri:
                                return(str(octogone)+' '+str(hexagone)+' '+str(triangle)+' '+str(heptagone))

def octo_hex_tri_pent(octogones, hexagones, triangles, pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_hex:
                        last_two_tri = str(triangle)[2:]
                        for pentagone in pentagones:
                            if str(pentagone)[:2] == last_two_tri:
                                return(str(octogone)+' '+str(hexagone)+' '+str(triangle)+' '+str(pentagone))

def octo_hex_pent(octogones, hexagones, pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_hex:
                        last_two_pent = str(pentagone)[2:]
                        return(str(octogone)+' '+str(hexagone)+' '+str(pentagone))

def octo_hex_pent_tri(octogones, hexagones,pentagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_hex:
                        last_two_pent = str(pentagone)[2:]
                        for triangle in triangles:
                            if str(triangle)[:2] == last_two_pent:
                                return (str(octogone)+' '+str(hexagone)+' '+str(pentagone)+' '+str(triangle))

def octo_hex_hept_pent(octogones, hexagones, heptagones,pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for heptagone in heptagones:
                    if str(heptagone)[:2] == last_two_hex:
                        last_two_hept = str(heptagone)[2:]
                        for pentagone in pentagones:
                            if str(pentagone)[:2] == last_two_hept:
                                return(str(octogone)+' '+str(hexagone)+' '+str(heptagone)+' '+str(pentagone))

def octo_hex_hept_square(octogones, hexagones, heptagones,squares):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for heptagone in heptagones:
                    if str(heptagone)[:2] == last_two_hex:
                        last_two_hept = str(heptagone)[2:]
                        for square in squares:
                            if str(square)[:2] == last_two_hept:
                                return(str(octogone)+' '+str(hexagone)+' '+str(heptagone)+' '+str(square))

def octo_hex_hept_tri(octogones, hexagones, heptagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for heptagone in heptagones:
                    if str(heptagone)[:2] == last_two_hex:
                        last_two_hept = str(heptagone)[2:]
                        for triangle in triangles:
                            if str(triangle)[:2] == last_two_hept:
                                return(str(octogone)+' '+str(hexagone)+' '+str(heptagone)+' '+str(triangle))

def octo_hex_hept_oct(octogones, hexagones, heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for heptagone in heptagones:
                    if str(heptagone)[:2] == last_two_hex:
                        last_two_hept = str(heptagone)[2:]
                        for octo in octogones:
                            if str(octo)[:2] == last_two_hept:
                                return(str(octogone)+' '+str(hexagone)+' '+str(heptagone)+' '+str(octo))

def octo_hex_pent_square(octogones, hexagones,pentagones,squares):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_hex:
                        last_two_pent = str(pentagone)[2:]
                        for square in squares:
                            if str(square)[:2] == last_two_pent:
                                return (str(octogone)+' '+str(hexagone)+' '+str(pentagone)+' '+str(square))

def octo_hex_pent_hept(octogones, hexagones,pentagones,heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_hex:
                        last_two_pent = str(pentagone)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_pent:
                                return (str(octogone)+' '+str(hexagone)+' '+str(pentagone)+' '+str(heptagone))

def octo_hex_square_tri(octogones, hexagones, squares,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_hex:
                        last_two_square = str(square)[2:]
                        for triangle in triangles:
                            if str(triangle)[:2] == last_two_square:
                                return (str(octogone)+' '+str(hexagone)+' '+str(square)+' '+str(triangle))

def octo_hex_square_pent_tri(octogones, hexagones, squares,pentagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_hex:
                        last_two_square = str(square)[2:]
                        for pentagone in pentagones:
                            if str(pentagone)[:2] == last_two_square:
                                last_two_pent = str(pentagone)[2:]
                                for triangle in triangles:
                                    if str(triangle)[:2] == last_two_pent:
                                        return (str(octogone)+' '+str(hexagone)+' '+str(square)+' '+str(pentagone)+' '+str(triangle))

def octo_hex_square_pent_hept(octogones, hexagones, squares,pentagones,heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_hex:
                        last_two_square = str(square)[2:]
                        for pentagone in pentagones:
                            if str(pentagone)[:2] == last_two_square:
                                last_two_pent = str(pentagone)[2:]
                                for heptagone in heptagones:
                                    if str(heptagone)[:2] == last_two_pent:
                                        return (str(octogone)+' '+str(hexagone)+' '+str(square)+' '+str(pentagone)+' '+str(heptagone))

def octo_hex_square_tri_pent(octogones, hexagones, squares,triangles, pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_hex:
                        last_two_square = str(square)[2:]
                        for triangle in triangles:
                            if str(triangle)[:2] == last_two_square:
                                last_two_tri = str(triangle)[2:]
                                for pentagone in pentagones:
                                    if str(pentagone)[:2] == last_two_tri:
                                        return (str(octogone)+' '+str(hexagone)+' '+str(square)+' '+str(triangle)+' '+str(pentagone))

def octo_hex_square_tri_hept(octogones, hexagones, squares,triangles, heptagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_hex:
                        last_two_square = str(square)[2:]
                        for triangle in triangles:
                            if str(triangle)[:2] == last_two_square:
                                last_two_tri = str(triangle)[2:]
                                for heptagone in heptagones:
                                    if str(heptagone)[:2] == last_two_tri:
                                        return (str(octogone)+' '+str(hexagone)+' '+str(square)+' '+str(triangle)+' '+str(heptagone))

def octo_hex_square_hept_tri(octogones, hexagones, squares,heptagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_hex:
                        last_two_square = str(square)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_square:
                                last_two_hept = str(heptagone)[2:]
                                for triangle in triangles:
                                    if str(triangle)[:2] == last_two_hept:
                                        return (str(octogone)+' '+str(hexagone)+' '+str(square)+' '+str(heptagone)+' '+str(triangle))

def octo_hex_square_hept_pent(octogones, hexagones, squares,heptagones,pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for hexagone in hexagones:
            if str(hexagone)[:2] == last_two_octo:
                last_two_hex = str(hexagone)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_hex:
                        last_two_square = str(square)[2:]
                        for heptagone in heptagones:
                            if str(heptagone)[:2] == last_two_square:
                                last_two_hept = str(heptagone)[2:]
                                for pentagone in pentagones:
                                    if str(pentagone)[:2] == last_two_hept:
                                        return (str(octogone)+' '+str(hexagone)+' '+str(square)+' '+str(heptagone)+' '+str(pentagone))

def octo_hept_hexa(octogones, heptagones,hexagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for heptagone in heptagones:
            if str(heptagone)[:2] == last_two_octo:
                last_two_hept = str(heptagone)[2:]
                for hexagone in hexagones:
                    if str(hexagone)[:2] == last_two_hept:
                        return(str(octogone)+' '+str(heptagone)+' '+str(hexagone))

def octo_hept_pent(octogones, heptagones,pentagones):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for heptagone in heptagones:
            if str(heptagone)[:2] == last_two_octo:
                last_two_hept = str(heptagone)[2:]
                for pentagone in pentagones:
                    if str(pentagone)[:2] == last_two_hept:
                        return(str(octogone)+' '+str(heptagone)+' '+str(pentagone))

def octo_hept_square(octogones, heptagones,squares):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for heptagone in heptagones:
            if str(heptagone)[:2] == last_two_octo:
                last_two_hept = str(heptagone)[2:]
                for square in squares:
                    if str(square)[:2] == last_two_hept:
                        return(str(octogone)+' '+str(heptagone)+' '+str(square))

def octo_hept_tri(octogones, heptagones,triangles):
    for octogone in octogones:
        last_two_octo = str(octogone)[2:]
        for heptagone in heptagones:
            if str(heptagone)[:2] == last_two_octo:
                last_two_hept = str(heptagone)[2:]
                for triangle in triangles:
                    if str(triangle)[:2] == last_two_hept:
                        return(str(octogone)+' '+str(heptagone)+' '+str(triangle))

def hex_hept_octo(hexagones, heptagones, octogones):
    for hexagone in hexagones:
        last_two_hex = str(hexagone)[2:]
        for heptagone in heptagones:
            if str(heptagone)[:2] == last_two_hex:
                last_two_hept = str(heptagone)[2:]
                for octogone in octogones:
                    if str(octogone)[:2] == last_two_hept:
                        return(str(hexagone)+' '+str(heptagone)+' '+str(octogone))

def find_special_set():
    #specials = {} #This dictionary should store the set of numbers we are interested in

    triangles = {triangle_number(x):x for x in range(1,141) if len(str(int(triangle_number(x)))) == 4 }
    squares = {square_number(x):x for x in range(1,100) if len(str(int(square_number(x)))) == 4 }
    pentagones = {pentagonal_number(x):x for x in range(1,82) if len(str(int(pentagonal_number(x)))) == 4 }
    hexagones = {hexagonal_number(x):x for x in range(1,71) if len(str(int(hexagonal_number(x)))) == 4 }
    heptagones = {heptagonal_number(x):x for x in range(1,64) if len(str(int(heptagonal_number(x)))) == 4 }
    octogones = {octagonal_number(x):x for x in range(1,59) if len(str(int(octagonal_number(x)))) == 4 }

    #Starts with 8
    print(octo_hex_hept_tri(octogones, hexagones, heptagones,triangles)) # Doesn't work
    print(octo_hex_hept_square(octogones, hexagones, heptagones,squares)) # Doesn't work
    print(octo_hex_hept_pent(octogones, hexagones, heptagones,pentagones)) # Doesn't work  

    print(octo_hex_pent_square(octogones, hexagones,pentagones,squares)) #Doesn't work
    print(octo_hex_pent_tri(octogones, hexagones,pentagones,triangles)) #Doesn't work
    print(octo_hex_pent_hept(octogones, hexagones,pentagones,heptagones)) #Doesn't work

    print(octo_hex_square_tri(octogones, hexagones, squares, triangles)) #Work
    print(octo_hex_square_tri_pent(octogones, hexagones, squares, triangles, pentagones)) #Doesn't work
    print(octo_hex_square_tri_hept(octogones, hexagones, squares, triangles, heptagones)) #Doesn't work

    print(octo_hex_square_pent_tri(octogones, hexagones, squares, pentagones,triangles)) #Doesn't Work
    print(octo_hex_square_pent_hept(octogones, hexagones, squares, pentagones,heptagones)) #Doesn't Work
    
    print(octo_hex_square_hept_tri(octogones, hexagones, squares, heptagones,triangles)) #Doesn't work
    print(octo_hex_square_hept_pent(octogones, hexagones, squares, heptagones,pentagones)) #Doesn't work
    
    print(octo_hex_tri(octogones, hexagones, triangles)) #Work
    print(octo_hex_tri_square(octogones, hexagones, triangles,squares)) #Doesn't work
    print(octo_hex_tri_hept(octogones, hexagones, triangles, heptagones)) #Doesn't work
    print(octo_hex_tri_pent(octogones, hexagones, triangles, pentagones)) #Doesn't work

    print(octo_hept_hexa(octogones,heptagones,hexagones)) #Doesn't work
    print(octo_hept_pent(octogones,heptagones,pentagones)) #Doesn't work
    print(octo_hept_square(octogones,heptagones,squares)) #Doesn't work
    print(octo_hept_tri(octogones,heptagones,triangles)) #Doesn't work   

    print(octo_pent(octogones,pentagones)) #Work
    print(octo_pent_tri(octogones,pentagones,triangles)) #Doesn't work 
    print(octo_pent_square(octogones,pentagones,squares)) #Doesn't work 
    print(octo_pent_hex(octogones,pentagones,hexagones)) #Doesn't work 
    print(octo_pent_hept(octogones,pentagones,heptagones)) #Doesn't work 

    print(octo_tri(octogones,triangles)) #Work
    print(octo_tri_pent(octogones, triangles, pentagones)) #Doesn't work 
    print(octo_tri_hex(octogones, triangles, hexagones)) #Doesn't work 
    print(octo_tri_hept(octogones, triangles, heptagones)) #Doesn't work 
    print(octo_tri_square(octogones, triangles, squares)) #Doesn't work 
    
    print(octo_square(octogones,squares)) #Work
    print(octo_square_tri(octogones, squares, triangles)) #Work
    print(octo_square_tri_pent(octogones, squares, triangles,pentagones)) #Doesn't work
    print(octo_square_tri_hex(octogones, squares, triangles,hexagones)) #Doesn't work
    print(octo_square_tri_hept(octogones, squares, triangles,heptagones)) #Doesn't work

    print(octo_square_pent(octogones, squares, pentagones)) #Work
    print(octo_square_pent_tri(octogones, squares, pentagones,triangles)) #Doesn't work
    print(octo_square_pent_hex(octogones, squares, pentagones,hexagones)) #Doesn't work
    print(octo_square_pent_hept(octogones, squares, pentagones,heptagones)) #Doesn't work

    print(octo_square_hex(octogones, squares, hexagones)) #Work
    print(octo_square_hex_tri(octogones, squares, hexagones,triangles)) #Work
    print(octo_square_hex_tri_pent(octogones, squares, hexagones,triangles,pentagones)) #Doesn't work
    print(octo_square_hex_tri_hept(octogones, squares, hexagones,triangles,heptagones)) #Doesn't work
    print(octo_square_hex_pent(octogones, squares, hexagones,pentagones)) #Work
    print(octo_square_hex_pent_tri(octogones, squares, hexagones,pentagones,triangles)) #Doesn't work
    print(octo_square_hex_pent_hept(octogones, squares, hexagones,pentagones,heptagones)) #Doesn't work
    print(octo_square_hex_hept(octogones, squares, hexagones,heptagones)) #Work
    print(octo_square_hex_hept_tri(octogones, squares, hexagones,heptagones, triangles)) #Doesn't work
    print(octo_square_hex_hept_pent(octogones, squares, hexagones,heptagones, pentagones)) #Work
    print(octo_square_hex_hept_pent_tri(octogones, squares, hexagones,heptagones, pentagones,triangles)) #Work

    #print(len(octogones))
    #print(len(heptagones))
    #print(len(hexagones))
    #print(len(pentagones))
    #print(len(squares))
    #print(len(triangles))
    #print(len(octogones) + len(heptagones) + len(hexagones) + len(pentagones) + len(squares) + len(triangles))
    #print(len({**octogones , ** heptagones, ** hexagones, ** pentagones, ** squares, ** triangles}))

find_special_set()