import itertools

#We create all the functions to find the relevant numbers
def triangle_number(n):
    return int((n * (n+1))/2)

def square_number(n):
    return int(n*n)

def pentagonal_number(n):
    return int((n * (3*n - 1)) / 2)

def hexagonal_number(n):
    return int(n * (2*n - 1))

def heptagonal_number(n):
    return int((n * (5*n - 3))/2)

def octagonal_number(n):
    return int(n * (3*n - 2))

def test_options(list_1, list_2, list_3,list_4,list_5,list_6):
    for num1 in list_1:
        last_two_1 = str(num1)[2:]
        for num2 in list_2:
            if str(num2)[:2] == last_two_1:
                last_two_2 = str(num2)[2:]
                for num3 in list_3:
                    if str(num3)[:2] == last_two_2:
                        last_two_3 = str(num3)[2:]
                        for num4 in list_4:
                            if str(num4)[:2] == last_two_3:
                                last_two_4 = str(int(num4))[2:]
                                for num5 in list_5:
                                    if str(int(num5))[:2] == last_two_4:
                                        last_two_5 = str(int(num5))[2:]
                                        for num6 in list_6:
                                            if str(int(num6))[:2] == last_two_5:
                                                last_two_6 = str(num6)[2:]
                                                if str(int(num1))[:2] == last_two_6:
                                                    print(str(num1)+' '+str(num2)+' '+str(num3)+' '+str(num4)+' '+str(num5)+' '+str(num6))
                                                    print(num1+num2+num3+num4+num5+num6)

if __name__ == "__main__":
    triangles = {triangle_number(x):x for x in range(1,141) if len(str(int(triangle_number(x)))) == 4 }
    squares = {square_number(x):x for x in range(1,100) if len(str(int(square_number(x)))) == 4 }
    pentagones = {pentagonal_number(x):x for x in range(1,82) if len(str(int(pentagonal_number(x)))) == 4 }
    hexagones = {hexagonal_number(x):x for x in range(1,71) if len(str(int(hexagonal_number(x)))) == 4 }
    heptagones = {heptagonal_number(x):x for x in range(1,64) if len(str(int(heptagonal_number(x)))) == 4 }
    octogones = {octagonal_number(x):x for x in range(1,59) if len(str(int(octagonal_number(x)))) == 4 }

    all_num = {'3':triangles,'4':squares,'5':pentagones,'6':hexagones,'7':heptagones,'8':octogones}
    permutations = list((itertools.permutations("345678")))
    
    for permut in permutations:
        test_options(all_num[permut[0]],all_num[permut[1]],all_num[permut[2]],all_num[permut[3]],all_num[permut[4]],all_num[permut[5]])